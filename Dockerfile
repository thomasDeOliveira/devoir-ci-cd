# Utilisez une image de base légère avec un serveur web, par exemple Nginx
FROM nginx:latest

# Définissez le répertoire de travail dans le conteneur
WORKDIR /usr/share/nginx/html

# Copiez le contenu de votre site web dans le conteneur
COPY ./mon_site /usr/share/nginx/html

# Exposez le port 80, le port par défaut pour les serveurs web
EXPOSE 80

# Définissez le point d'entrée du conteneur
CMD ["nginx", "-g", "daemon off;"]
