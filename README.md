# Devoir Ci-CD

## Exercice 0:

Pour construire le conteneur docker j'ai utilisé le Dockerfile dans laquelle je lui ai mis une image ici nginx
Un repertoire de travail, je lui ai dis de copier l'endroit ou est ma page statique dans la zone de travail et je lui ai donné un port.

Ensuite dans mon environnement Linux, j'ai fais la commande depuis le dossier DEVOIR-CI-CD comme ca avec juste le . la commande a pu trouver le dockerfile :

docker build -t mon-site-web .

Enfin pour lancer le conteneur j'ai fais cette commande :
docker run -p 8080:80 -v ./mon_site:/usr/share/nginx/html mon-site-web

C'est le -v qui me permet d'avoir un volume modulable.

## Exercice 1: